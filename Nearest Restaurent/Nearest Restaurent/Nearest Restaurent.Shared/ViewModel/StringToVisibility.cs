﻿using System;
using System.Collections.Generic;
using System.Text;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Data;

namespace Nearest_Restaurent.ViewModel
{
    public sealed class StringToVisibility : IValueConverter
    {
       public object Convert(object value, Type targetType, object parameter, string language)
        {
            if (value is string && value == "")
            {
                return Visibility.Collapsed;
            }
            else
                return Visibility.Visible;
        }

       public object ConvertBack(object value, Type targetType, object parameter, string language)
        {
            return 0;
        }
    }
}
