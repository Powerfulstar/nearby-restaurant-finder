﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Devices.Geolocation;


namespace Nearest_Restaurent.ViewModel
{
    public class CategoryList
    {
        public string id { get; set; }
        public string name { get; set; }
    }

    public class Location
    {
        public string street { get; set; }
        public string city { get; set; }
        public string country { get; set; }
        public string zip { get; set; }
        public double latitude { get; set; }
        public double longitude { get; set; }
        public string state { get; set; }
        public long? located_in { get; set; }
        public double Distance { get; set; }
        public Geopoint MapCoOrdinate { get; set; }
    }

    public class Datum
    {
        public string category { get; set; }
        public List<CategoryList> category_list { get; set; }
        public Location location { get; set; }
        public string name { get; set; }
        public string id { get; set; }        
    }

    public class Paging
    {
        public string next { get; set; }
    }

    public class RootObject
    {
        public List<Datum> data { get; set; }
        public Paging paging { get; set; }       
               
    }

    public class MapCoOrdinate
    {
        public Geopoint geopoint { get; set; }
        public string Name { get; set; }
        public string Id { get; set; }
    }
}
