﻿using GalaSoft.MvvmLight;
using Nearest_Restaurent.ViewModel.some;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Text.RegularExpressions;
using Windows.Devices.Geolocation;
using Nearest_Restaurent.Helpers;
namespace Nearest_Restaurent.ViewModel
{
    class PageDetailViewModel : ViewModelBase
    {

        #region Property PageDetails

        /// <summary>
        /// The <see cref="PageDetailsProperty" /> property's name.
        /// </summary>
        public const string PageDetailsPropertyPropertyName = "PageDetailsProperty";

        private PageDetails _pagedetailsProperty = new PageDetails();

        /// <summary>
        /// Sets and gets the PageDetailsProperty property.
        /// Changes to that property's value raise the PropertyChanged event. 
        /// </summary>
        public PageDetails PageDetailsProperty
        {
            get
            {
                return _pagedetailsProperty;
            }

            set
            {
                if (_pagedetailsProperty == value)
                {
                    return;
                }
                _pagedetailsProperty = value;
                RaisePropertyChanged(PageDetailsPropertyPropertyName);
            }
        }

        #endregion

        #region Property PageID
        /// <summary>
        /// The <see cref="PageID" /> property's name.
        /// </summary>
        public const string PageIDPropertyName = "PageID";

        private string _pageID = string.Empty;

        /// <summary>
        /// Sets and gets the PageID property.
        /// Changes to that property's value raise the PropertyChanged event. 
        /// </summary>
        public string PageID
        {
            get
            {
                return _pageID;
            }

            set
            {
                if (_pageID == value)
                {
                    return;
                }                
                _pageID = value;
                RaisePropertyChanged(PageIDPropertyName);
                LoadData(PageID);
            }
        }
        #endregion

        #region Property Center
        /// <summary>
        /// The <see cref="Center" /> property's name.
        /// </summary>
        public const string CenterPropertyName = "Center";
        private Geopoint _center;
        

        /// <summary>
        /// Sets and gets the Center property.
        /// Changes to that property's value raise the PropertyChanged event. 
        /// </summary>
        public Geopoint Center
        {
            get
            {
                return _center;
            }

            set
            {
                if (_center == value)
                {
                    return;
                }
                _center = value;
                RaisePropertyChanged(CenterPropertyName);
            }
        }
        #endregion

        #region Property MyCenter
        /// <summary>
        /// The <see cref="MyCenter" /> property's name.
        /// </summary>
        public const string MyCenterPropertyName = "MyCenter";
        private Geopoint _mycenter;

        /// <summary>
        /// Sets and gets the Center property.
        /// Changes to that property's value raise the PropertyChanged event. 
        /// </summary>
        public Geopoint MyCenter
        {
            get
            {
                return _mycenter;
            }

            set
            {
                if (_mycenter == value)
                {
                    return;
                }
                _mycenter = value;
                RaisePropertyChanged(MyCenterPropertyName);
            }
        }
        #endregion


        
        
    
        
        
        
        
        
        #region Property IsAboutAvailable
        /// <summary>
        /// The <see cref="IsAboutAvailable" /> property's name.
        /// </summary>
        public const string IsAboutAvailablePropertyName = "IsAboutAvailable";

        private bool _IsAboutAvailable = true;

        /// <summary>
        /// Sets and gets the IsAboutAvailable property.
        /// Changes to that property's value raise the PropertyChanged event. 
        /// </summary>
        public bool IsAboutAvailable
        {
            get
            {
                return _IsAboutAvailable;
            }

            set
            {
                if (_IsAboutAvailable == value)
                {
                    return;
                }
                _IsAboutAvailable = value;
                RaisePropertyChanged(IsAboutAvailablePropertyName);
            }
        }
        #endregion

        #region Property IsOpeningHourAvaiable
        /// <summary>
        /// The <see cref="IsOpeningHourAvaiable" /> property's name.
        /// </summary>
        public const string IsOpeningHourAvaiablePropertyName = "IsOpeningHourAvaiable";

        private bool _IsOpeningHourAvaiable = false;

        /// <summary>
        /// Sets and gets the IsOpeningHourAvaiable property.
        /// Changes to that property's value raise the PropertyChanged event. 
        /// </summary>
        public bool IsOpeningHourAvaiable
        {
            get
            {
                return _IsOpeningHourAvaiable;
            }

            set
            {
                if (_IsOpeningHourAvaiable == value)
                {
                    return;
                }
                _IsOpeningHourAvaiable = value;
                RaisePropertyChanged(IsOpeningHourAvaiablePropertyName);
            }
        }
        #endregion
        #region Property IsSpecialtiesAvailable
        /// <summary>
        /// The <see cref="IsSpecialtiesAvailable" /> property's name.
        /// </summary>
        public const string IsSpecialtiesAvailablePropertyName = "IsSpecialtiesAvailable";

        private bool _IsSpecialtiesAvailable = false;

        /// <summary>
        /// Sets and gets the IsSpecialtiesAvailable property.
        /// Changes to that property's value raise the PropertyChanged event. 
        /// </summary>
        public bool IsSpecialtiesAvailable
        {
            get
            {
                return _IsSpecialtiesAvailable;
            }

            set
            {
                if (_IsSpecialtiesAvailable == value)
                {
                    return;
                }
                _IsSpecialtiesAvailable = value;
                RaisePropertyChanged(IsSpecialtiesAvailablePropertyName);
            }
        }
        #endregion
        #region Property IsServicesAvailable
        /// <summary>
        /// The <see cref="IsServicesAvailable" /> property's name.
        /// </summary>
        public const string IsServicesAvailablePropertyName = "IsServicesAvailable";

        private bool _IsServicesAvailable = false;

        /// <summary>
        /// Sets and gets the IsServicesAvailable property.
        /// Changes to that property's value raise the PropertyChanged event. 
        /// </summary>
        public bool IsServicesAvailable
        {
            get
            {
                return _IsServicesAvailable;
            }

            set
            {
                if (_IsServicesAvailable == value)
                {
                    return;
                }
                _IsServicesAvailable = value;
                RaisePropertyChanged(IsServicesAvailablePropertyName);
            }
        }
        #endregion
        #region Property IsPaymentAvailable
        /// <summary>
        /// The <see cref="IsPaymentAvailable" /> property's name.
        /// </summary>
        public const string IsPaymentAvailablePropertyName = "IsPaymentAvailable";

        private bool _IsPaymentAvailable = false;

        /// <summary>
        /// Sets and gets the IsPaymentAvailable property.
        /// Changes to that property's value raise the PropertyChanged event. 
        /// </summary>
        public bool IsPaymentAvailable
        {
            get
            {
                return _IsPaymentAvailable;
            }

            set
            {
                if (_IsPaymentAvailable == value)
                {
                    return;
                }
                _IsPaymentAvailable = value;
                RaisePropertyChanged(IsPaymentAvailablePropertyName);
            }
        }
        #endregion
        #region Property IsParkingAvailable
        /// <summary>
        /// The <see cref="IsParkingAvailable" /> property's name.
        /// </summary>
        public const string IsParkingAvailablePropertyName = "IsParkingAvailable";

        private bool _IsParkingAvailable = false;

        /// <summary>
        /// Sets and gets the IsParkingAvailable property.
        /// Changes to that property's value raise the PropertyChanged event. 
        /// </summary>
        public bool IsParkingAvailable
        {
            get
            {
                return _IsParkingAvailable;
            }

            set
            {
                if (_IsParkingAvailable == value)
                {
                    return;
                }
                _IsParkingAvailable = value;
                RaisePropertyChanged(IsParkingAvailablePropertyName);
            }
        }
        #endregion
        #region Property IsPhoneNumberAvailable
        /// <summary>
        /// The <see cref="IsPhoneNumberAvailable" /> property's name.
        /// </summary>
        public const string IsPhoneNumberAvailablePropertyName = "IsPhoneNumberAvailable";

        private bool _IsPhoneNumberAvailable = false;

        /// <summary>
        /// Sets and gets the IsPhoneNumberAvailable property.
        /// Changes to that property's value raise the PropertyChanged event. 
        /// </summary>
        public bool IsPhoneNumberAvailable
        {
            get
            {
                return _IsPhoneNumberAvailable;
            }

            set
            {
                if (_IsPhoneNumberAvailable == value)
                {
                    return;
                }
                _IsPhoneNumberAvailable = value;
                RaisePropertyChanged(IsPhoneNumberAvailablePropertyName);
            }
        }
        #endregion
        #region Property IsWebsiteAvailable
        /// <summary>
        /// The <see cref="IsWebsiteAvailable" /> property's name.
        /// </summary>
        public const string IsWebsiteAvailablePropertyName = "IsWebsiteAvailable";

        private bool _IsWebsiteAvailable = false;

        /// <summary>
        /// Sets and gets the IsWebsiteAvailable property.
        /// Changes to that property's value raise the PropertyChanged event. 
        /// </summary>
        public bool IsWebsiteAvailable
        {
            get
            {
                return _IsWebsiteAvailable;
            }

            set
            {
                if (_IsWebsiteAvailable == value)
                {
                    return;
                }
                _IsWebsiteAvailable = value;
                RaisePropertyChanged(IsWebsiteAvailablePropertyName);
            }
        }
        #endregion
        #region Property IsLoading
        /// <summary>
        /// The <see cref="IsloadingPageDetails" /> property's name.
        /// </summary>
        public const string IsloadingPageDetailsPropertyName = "IsloadingPageDetails";

        private bool _IsloadingPageDetails = false;

        /// <summary>
        /// Sets and gets the IsloadingPageDetails property.
        /// Changes to that property's value raise the PropertyChanged event. 
        /// </summary>
        public bool IsloadingPageDetails
        {
            get
            {
                return _IsloadingPageDetails;
            }

            set
            {
                if (_IsloadingPageDetails == value)
                {
                    return;
                }
                _IsloadingPageDetails = value;
                RaisePropertyChanged(IsloadingPageDetailsPropertyName);
            }
        }
        #endregion
        #region Property IsAddressAvailable
        /// <summary>
        /// The <see cref="IsAddressAvailable" /> property's name.
        /// </summary>
        public const string IsAddressAvailablePropertyName = "IsAddressAvailable";

        private bool _IsAddressAvailable = false;

        /// <summary>
        /// Sets and gets the IsAddressAvailable property.
        /// Changes to that property's value raise the PropertyChanged event. 
        /// </summary>
        public bool IsAddressAvailable
        {
            get
            {
                return _IsAddressAvailable;
            }

            set
            {
                if (_IsAddressAvailable == value)
                {
                    return;
                }                
                _IsAddressAvailable = value;
                RaisePropertyChanged(IsAddressAvailablePropertyName);
            }
        }
        #endregion

        #region Property IsThereAnyAboutInfo
        /// <summary>
        /// The <see cref="IsThereAnyAboutInfo" /> property's name.
        /// </summary>
        public const string IsThereAnyAboutInfoPropertyName = "IsThereAnyAboutInfo";

        private bool _IsThereAnyAboutInfo = true;

        /// <summary>
        /// Sets and gets the IsThereAnyAboutInfo property.
        /// Changes to that property's value raise the PropertyChanged event. 
        /// </summary>
        public bool IsThereAnyAboutInfo
        {
            get
            {
                return _IsThereAnyAboutInfo;
            }

            set
            {
                if (_IsThereAnyAboutInfo == value)
                {
                    return;
                }
                _IsThereAnyAboutInfo = value;
                RaisePropertyChanged(IsThereAnyAboutInfoPropertyName);
            }
        }
        #endregion

        #region Property ErrorMessage
        /// <summary>
        /// The <see cref="ErroMeassage" /> property's name.
        /// </summary>
        public const string ErroMeassagePropertyName = "ErroMeassage";

        private string _ErroMeassage = string.Empty;

        /// <summary>
        /// Sets and gets the ErroMeassage property.
        /// Changes to that property's value raise the PropertyChanged event. 
        /// </summary>
        public string ErroMeassage
        {
            get
            {
                return _ErroMeassage;
            }

            set
            {
                if (_ErroMeassage == value)
                {
                    return;
                }
                _ErroMeassage = value;
                RaisePropertyChanged(ErroMeassagePropertyName);
            }
        }
        #endregion

        #region Property IsShowErrorMessage
        /// <summary>
        /// The <see cref="IsShowErrorMessage" /> property's name.
        /// </summary>
        public const string IsShowErrorMessagePropertyName = "IsShowErrorMessage";

        private bool _IsShowErrorMessage = false;

        /// <summary>
        /// Sets and gets the IsShowErrorMessage property.
        /// Changes to that property's value raise the PropertyChanged event. 
        /// </summary>
        public bool IsShowErrorMessage
        {
            get
            {
                return _IsShowErrorMessage;
            }

            set
            {
                if (_IsShowErrorMessage == value)
                {
                    return;
                }
                _IsShowErrorMessage = value;
                RaisePropertyChanged(IsShowErrorMessagePropertyName);
            }
        }
        #endregion
        public PageDetailViewModel()
        {
            if (IsInDesignMode)
            {
                LoadData("141440962557363");
            }
        }

        public async void LoadData(string ID)
        {
            IsloadingPageDetails = true;
            IsAboutAvailable = true;
            HttpClient client = new HttpClient();
            string Url = "http://graph.facebook.com/" + ID;
            //string Url = "http://graph.facebook.com/" + "141440962557363";

            try
            {
                string PageDetailsJson = await client.GetStringAsync(Url);
                PageDetailsProperty = JsonConvert.DeserializeObject<PageDetails>(PageDetailsJson);
                IsShowErrorMessage = false;


            }
            catch (Exception e)
            {
                if (e.Message == "net_http_message_not_success_statuscode")
                {
                    ErroMeassage = "There is something wrong with your data connection :( \nKindly check your wifi or cellular data";
                }
                IsShowErrorMessage = true;
                LoadData(ID);
            }

            IsloadingPageDetails = false;


            try
            {
                CheckProperties(PageDetailsProperty);
            }
            catch (Exception)
            {
                IsAboutAvailable = false;
            }
            

            BasicGeoposition position = new BasicGeoposition();
            position.Latitude = PageDetailsProperty.location.latitude;
            position.Longitude = PageDetailsProperty.location.longitude;

            Center = new Geopoint(position);

            BasicGeoposition position2 = new BasicGeoposition();
            position2.Latitude = (App.Current.Resources["Locator"] as ViewModelLocator).Main.MyBasicGeoPosition.Latitude;
            position2.Longitude = (App.Current.Resources["Locator"] as ViewModelLocator).Main.MyBasicGeoPosition.Longitude;



            MyCenter = new Geopoint(position2);
            try
            {
                PageDetailsProperty.hours.sat_1_open = PageDetailsProperty.hours.sat_1_open + " - " + PageDetailsProperty.hours.sat_1_close;
                PageDetailsProperty.hours.sun_1_open = PageDetailsProperty.hours.sun_1_open + " - " + PageDetailsProperty.hours.sun_1_close;
                PageDetailsProperty.hours.mon_1_open = PageDetailsProperty.hours.mon_1_open + " - " + PageDetailsProperty.hours.mon_1_close;
                PageDetailsProperty.hours.tue_1_open = PageDetailsProperty.hours.tue_1_open + " - " + PageDetailsProperty.hours.tue_1_close;
                PageDetailsProperty.hours.wed_1_open = PageDetailsProperty.hours.wed_1_open + " - " + PageDetailsProperty.hours.wed_1_close;
                PageDetailsProperty.hours.thu_1_open = PageDetailsProperty.hours.thu_1_open + " - " + PageDetailsProperty.hours.thu_1_close;
                PageDetailsProperty.hours.fri_1_open = PageDetailsProperty.hours.fri_1_open + " - " + PageDetailsProperty.hours.fri_1_close;
            }
            catch (Exception)
            {
                
                
            }
            try
            {
                if (PageDetailsProperty.features == null)
                {
                    PageDetailsProperty.features = new Features();   
                }                

                PageDetailsProperty.features.whatThe___breakfast = PageDetailsProperty.restaurant_specialties.breakfast == 1 ? "breakfast" : "";
                PageDetailsProperty.features.whatThe___coffee = PageDetailsProperty.restaurant_specialties.coffee == 1 ? "coffee" : "";
                PageDetailsProperty.features.whatThe___dinner = PageDetailsProperty.restaurant_specialties.dinner == 1 ? "dinner" : "";
                PageDetailsProperty.features.whatThe___drinks = PageDetailsProperty.restaurant_specialties.drinks == 1 ? "drinks" : "";
                PageDetailsProperty.features.whatThe___lunch = PageDetailsProperty.restaurant_specialties.lunch == 1 ? "lunch" : "";

                PageDetailsProperty.features.whatThe___street = PageDetailsProperty.parking.street == 1 ? "street" : "";
                PageDetailsProperty.features.whatThe___valet = PageDetailsProperty.parking.valet == 1 ? "valet" : "";
                PageDetailsProperty.features.whatThe___lot = PageDetailsProperty.parking.lot == 1 ? "lot" : "";


                PageDetailsProperty.features.whatThe___amex = PageDetailsProperty.payment_options.amex == 1 ? "amex" : "";
                PageDetailsProperty.features.whatThe___cash_only = PageDetailsProperty.payment_options.cash_only == 1 ? "cash_only" : "";
                PageDetailsProperty.features.whatThe___discover = PageDetailsProperty.payment_options.discover == 1 ? "discover" : "";
                PageDetailsProperty.features.whatThe___mastercard = PageDetailsProperty.payment_options.mastercard == 1 ? "mastercard" : "";
                PageDetailsProperty.features.whatThe___visa = PageDetailsProperty.payment_options.visa == 1 ? "visa" : "";


                PageDetailsProperty.features.whatThe___delivery = PageDetailsProperty.restaurant_services.delivery == 1 ? "delivery" : "";
                PageDetailsProperty.features.whatThe___catering = PageDetailsProperty.restaurant_services.catering == 1 ? "catering" : "";
                PageDetailsProperty.features.whatThe___groups = PageDetailsProperty.restaurant_services.groups == 1 ? "groups" : "";
                PageDetailsProperty.features.whatThe___kids = PageDetailsProperty.restaurant_services.kids == 1 ? "kids" : "";
                PageDetailsProperty.features.whatThe___outdoor = PageDetailsProperty.restaurant_services.outdoor == 1 ? "outdoor" : "";
                PageDetailsProperty.features.whatThe___reserve = PageDetailsProperty.restaurant_services.reserve == 1 ? "reserve" : "";
                PageDetailsProperty.features.whatThe___takeout = PageDetailsProperty.restaurant_services.takeout == 1 ? "takeout" : "";
                PageDetailsProperty.features.whatThe___waiter = PageDetailsProperty.restaurant_services.waiter == 1 ? "waiter" : "";
                PageDetailsProperty.features.whatThe___walkins = PageDetailsProperty.restaurant_services.walkins == 1 ? "walkins" : "";


                //if (PageDetailsProperty.features.PhoneNumbers == null)
                //{
                //    PageDetailsProperty.features.PhoneNumbers = new List<string>();
                //}

                //string[] digits = Regex.Split(PageDetailsProperty.phone, @"(\d{7}-\d{2})");
                //foreach (var value in digits)
                //{
                //    if (Regex.IsMatch(value, @"(\d{7}-\d{2})") || Regex.IsMatch(value, @"(\d{7}-\d{1})"))
                //    PageDetailsProperty.features.PhoneNumbers.Add(value);
                //}

                //digits = Regex.Split(PageDetailsProperty.phone, @"(\d{7})");
                //foreach (var value in digits)
                //{
                //    if (Regex.IsMatch(value, @"(\d{7})"))
                //    PageDetailsProperty.features.PhoneNumbers.Add(value);
                //}				



                //digits = Regex.Split(PageDetailsProperty.phone, @"(\d{11})");
                //foreach (var value in digits)
                //{
                //    if (Regex.IsMatch(value, @"(\d{11})"))
                //    PageDetailsProperty.features.PhoneNumbers.Add(value);
                //}



                //digits = Regex.Split(PageDetailsProperty.phone, @"(\d{7}-\d{1})");
                //foreach (var value in digits)
                //{
                //    if (Regex.IsMatch(value, @"(\d{7}-\d{1})"))
                //    PageDetailsProperty.features.PhoneNumbers.Add(value);
                //}
                
            }
            catch (Exception)
            {
                
                
            }
        }

     

        private void CheckProperties(PageDetails PageDetailsProperty)
        {
            if (PageDetailsProperty.location.street != null)
                IsAddressAvailable = true;
            else
                IsAddressAvailable = false;

            if (PageDetailsProperty.about != null)
                IsAboutAvailable = true;
            else
                IsAboutAvailable = false;

            if (PageDetailsProperty.hours != null)
                IsOpeningHourAvaiable = true;
            else
                IsOpeningHourAvaiable = false;

            if (PageDetailsProperty.restaurant_specialties != null)
                IsSpecialtiesAvailable = true;
            else
                IsSpecialtiesAvailable = false;

            if (PageDetailsProperty.restaurant_services != null)
                IsServicesAvailable = true;
            else
                IsServicesAvailable = false;

            if (PageDetailsProperty.payment_options != null)
                IsPaymentAvailable = true;
            else
                IsPaymentAvailable = false;

            if (PageDetailsProperty.parking != null)
            {
                if ( (PageDetailsProperty.parking.lot != 0 && PageDetailsProperty.parking.street !=0 && PageDetailsProperty.parking.valet != 0))                
                IsParkingAvailable = true;
            }
            else
                IsParkingAvailable = false;

            if (PageDetailsProperty.phone != null)
                IsPhoneNumberAvailable = true;
            else
                IsPhoneNumberAvailable = false;

            if (PageDetailsProperty.website != null)
                IsWebsiteAvailable = true;
            else
                IsWebsiteAvailable = false;
            
        }
    }
}
