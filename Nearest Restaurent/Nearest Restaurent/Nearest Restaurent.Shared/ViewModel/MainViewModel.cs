﻿using GalaSoft.MvvmLight;
using System;
using System.Linq;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Collections.ObjectModel;
using System.IO;
using Windows.Storage;
using Windows.Devices.Geolocation;
using Newtonsoft.Json;
using Nearest_Restaurent;
using GalaSoft.MvvmLight.Command;
using Windows.Devices.Geolocation.Geofencing;
using GalaSoft.MvvmLight.Messaging;
using Nearest_Restaurent.ViewModel.some;
using Windows.Services.Maps;
namespace Nearest_Restaurent.ViewModel
{
    class MainViewModel : ViewModelBase
    {


        #region Property Token

        private string _token;

        public string Token
        {
            get { return _token; }
            set { _token = value; }
        } 
        #endregion

        #region Property SearchKey
        /// <summary>
        /// The <see cref="SearchKey" /> property's name.
        /// </summary>
        public const string SearchKeyPropertyName = "SearchKey";

        private string _searchKey = "restaurant";

        /// <summary>
        /// Sets and gets the SearchKey property.
        /// Changes to that property's value raise the PropertyChanged event. 
        /// </summary>
        public string SearchKey
        {
            get
            {
                return _searchKey;
            }

            set
            {
                if (_searchKey == value)
                {
                    return;
                }                
                _searchKey = value;
                RaisePropertyChanged(SearchKeyPropertyName);
                LoadData();
            }
        }
        #endregion

        #region Property MyCenter
        /// <summary>
        /// The <see cref="MyCenter" /> property's name.
        /// </summary>
        public const string CenterPropertyName = "MyCenter";

        private string _center = string.Empty;

        /// <summary>
        /// Sets and gets the Center property.
        /// Changes to that property's value raise the PropertyChanged event. 
        /// </summary>
        public string MyCenter
        {
            get
            {
                return _center;
            }

            set
            {
                if (_center == value)
                {
                    return;
                }                
                _center = value;
                RaisePropertyChanged(CenterPropertyName);
            }
        }
        #endregion

        #region Property MyGeoPoint
        /// <summary>
        /// The <see cref="MyGeoPoint" /> property's name.
        /// </summary>
        public const string MyGeoPointPropertyName = "MyGeoPoint";

        private Geopoint _MyGeoPoint;

        /// <summary>
        /// Sets and gets the MyGeoPoint property.
        /// Changes to that property's value raise the PropertyChanged event. 
        /// </summary>
        public Geopoint MyGeoPoint
        {
            get
            {
                return _MyGeoPoint;
            }

            set
            {
                if (_MyGeoPoint == value)
                {
                    return;
                }
                _MyGeoPoint = value;
                RaisePropertyChanged(MyGeoPointPropertyName);
            }
        }
        #endregion

        #region Property MyBasicGeoiposition
        public BasicGeoposition MyBasicGeoPosition { get; set; }
        #endregion

        #region Property Type
        /// <summary>
        /// The <see cref="Type" /> property's name.
        /// </summary>
        public const string TypePropertyName = "Type";

        private string _type = string.Empty;

        /// <summary>
        /// Sets and gets the Type property.
        /// Changes to that property's value raise the PropertyChanged event. 
        /// </summary>
        public string Type
        {
            get
            {
                return _type;
            }

            set
            {
                if (_type == value)
                {
                    return;
                }                
                _type = value;
                RaisePropertyChanged(TypePropertyName);
            }
        }
        #endregion

        #region Property Distance
        /// <summary>
        /// The <see cref="Distance" /> property's name.
        /// </summary>
        public const string DistancePropertyName = "Distance";

        private string _distance = string.Empty;

        /// <summary>
        /// Sets and gets the Distance property.
        /// Changes to that property's value raise the PropertyChanged event. 
        /// </summary>
        public string Distance
        {
            get
            {
                return _distance;
            }

            set
            {
                if (_distance == value)
                {
                    return;
                }                
                _distance = value;
                RaisePropertyChanged(DistancePropertyName);
            }
        }
        #endregion

        #region Property SearchUrl
        /// <summary>
        /// The <see cref="SearchUrl" /> property's name.
        /// </summary>
        public const string SearchUrlPropertyName = "SearchUrl";

        private string _searchUrl = string.Empty;

        /// <summary>
        /// Sets and gets the SearchUrl property.
        /// Changes to that property's value raise the PropertyChanged event. 
        /// </summary>
        public string SearchUrl
        {
            get
            {
                return _searchUrl;
            }

            set
            {
                if (_searchUrl == value)
                {
                    return;
                }
                _searchUrl = value;              
                RaisePropertyChanged(SearchUrlPropertyName);
            }
        }
        #endregion

        #region Property Catagory_List
        /// <summary>
        /// The <see cref="Catagory_Suggestion" /> property's name.
        /// </summary>
        public const string Catagory_SuggestionPropertyName = "Catagory_Suggestion";

        private ObservableCollection<string> _catagory_Suggestion = new ObservableCollection<string>() { "American Restaurant", "Asian Restaurant", "Bakery", "Barbecue", "Barbecue Restaurant", "Breakfast and Brunch", "Burger Restaurant", "Burgers Restaurant", "Cafe", "Cafeteria", "Candy Store", "Cantonese Restaurant", "Chinese Restaurant", "Coffee Shop", "Continental Restaurant", "Dessert Restaurant", "Family Style Restaurant", "Fast Food", "Fast Food Restaurant", "Fish & Chips Shop", "Food Stand", "Food/Beverages", "French Restaurant", "Health Food Restaurant", "Health Food Store", "Hotel", "Ice Cream", "Indian Restaurant", "International Restaurant", "Italian Restaurant", "Japanese Restaurant", "Korean Restaurant", "Lounge", "Mediterranean Restaurant", "Mexican Restaurant", "Middle Eastern Restaurant", "Pizza Place", "Resort", "Restaurant", "Restaurant Supply", "Seafood Restaurant", "Smoothie & Juice Bar", "Steak ", "Sushi", "Take Out Restaurant", "Thai Restaurant", "Vegetarian Restaurant" };

        /// <summary>
        /// Sets and gets the Catagory_Suggestion property.
        /// Changes to that property's value raise the PropertyChanged event. 
        /// </summary>
        public ObservableCollection<string> Catagory_Suggestion
        {
            get
            {
                return _catagory_Suggestion;
            }

            set
            {
                if (_catagory_Suggestion == value)
                {
                    return;
                }
                
                _catagory_Suggestion = value;
                RaisePropertyChanged(Catagory_SuggestionPropertyName);
            }
        }
        #endregion

        #region Property RootObject
        /// <summary>
        /// The <see cref="RootObject" /> property's name.
        /// </summary>
        public const string RootObjectPropertyName = "RootObject";

        private RootObject _rootObject = new RootObject();

        /// <summary>
        /// Sets and gets the RootObject property.
        /// Changes to that property's value raise the PropertyChanged event. 
        /// </summary>
        public RootObject RootObject
        {
            get
            {
                return _rootObject;
            }

            set
            {
                if (_rootObject == value)
                {
                    return;
                }                
                _rootObject = value;
                RaisePropertyChanged(RootObjectPropertyName);
            }
        }
        #endregion

        #region Property SelectedIndex
        /// <summary>
        /// The <see cref="SelectedIndex" /> property's name.
        /// </summary>
        public const string SelectedIndexPropertyName = "SelectedIndex";

        private int _selectedIndex = 0;

        /// <summary>
        /// Sets and gets the SelectedIndex property.
        /// Changes to that property's value raise the PropertyChanged event. 
        /// </summary>
        public int SelectedIndex
        {
            get
            {
                return _selectedIndex;
            }

            set
            {
                if (_selectedIndex == value)
                {
                    return;
                }
                _selectedIndex = value;
                RaisePropertyChanged(SelectedIndexPropertyName);
            }
        }
        #endregion

        #region Property IsNotMainPivotVisible
        /// <summary>
        /// The <see cref="IsNotMainPivotVisible" /> property's name.
        /// </summary>
        public const string IsNotMainPivotVisiblePropertyName = "IsNotMainPivotVisible";

        private bool _mainpivotvisible = true;

        /// <summary>
        /// Sets and gets the IsNotMainPivotVisible property.
        /// Changes to that property's value raise the PropertyChanged event. 
        /// </summary>
        public bool IsNotMainPivotVisible
        {
            get
            {
                return _mainpivotvisible;
            }

            set
            {
                if (_mainpivotvisible == value)
                {
                    return;
                }                
                _mainpivotvisible = value;
                RaisePropertyChanged(IsNotMainPivotVisiblePropertyName);
            }
        }
        #endregion

        #region Property IsDetectingLocation
        /// <summary>
        /// The <see cref="IsDetectingLocation" /> property's name.
        /// </summary>
        public const string IsLocationDetectedPropertyName = "IsDetectingLocation";

        private bool _islocationdeted = false;

        /// <summary>
        /// Sets and gets the IsLocationDetected property.
        /// Changes to that property's value raise the PropertyChanged event. 
        /// </summary>
        public bool IsDetectingLocation
        {
            get
            {
                return _islocationdeted;
            }

            set
            {
                if (_islocationdeted == value)
                {
                    return;
                }                
                _islocationdeted = value;
                RaisePropertyChanged(IsLocationDetectedPropertyName);
            }
        }
        #endregion

        #region Property CivicAddress
        /// <summary>
        /// The <see cref="CivicAddress" /> property's name.
        /// </summary>
        public const string CivicAddressPropertyName = "CivicAddress";

        private string civicaddress = string.Empty;

        /// <summary>
        /// Sets and gets the CivicAddress property.
        /// Changes to that property's value raise the PropertyChanged event. 
        /// </summary>
        public string CivicAddress
        {
            get
            {
                return civicaddress;
            }

            set
            {
                if (civicaddress == value)
                {
                    return;
                }
                civicaddress = value;
                RaisePropertyChanged(CivicAddressPropertyName);
            }
        }
        #endregion

        #region Property ErrorMessage
        /// <summary>
        /// The <see cref="ErroMeassage" /> property's name.
        /// </summary>
        public const string ErroMeassagePropertyName = "ErroMeassage";

        private string _ErroMeassage = string.Empty;

        /// <summary>
        /// Sets and gets the ErroMeassage property.
        /// Changes to that property's value raise the PropertyChanged event. 
        /// </summary>
        public string ErroMeassage
        {
            get
            {
                return _ErroMeassage;
            }

            set
            {
                if (_ErroMeassage == value)
                {
                    return;
                }
                _ErroMeassage = value;
                RaisePropertyChanged(ErroMeassagePropertyName);
            }
        }
        #endregion

        #region Property IsShowErrorMessage
        /// <summary>
        /// The <see cref="IsShowErrorMessage" /> property's name.
        /// </summary>
        public const string IsShowErrorMessagePropertyName = "IsShowErrorMessage";

        private bool _IsShowErrorMessage = false;

        /// <summary>
        /// Sets and gets the IsShowErrorMessage property.
        /// Changes to that property's value raise the PropertyChanged event. 
        /// </summary>
        public bool IsShowErrorMessage
        {
            get
            {
                return _IsShowErrorMessage;
            }

            set
            {
                if (_IsShowErrorMessage == value)
                {
                    return;
                }
                _IsShowErrorMessage = value;
                RaisePropertyChanged(IsShowErrorMessagePropertyName);
            }
        }
        #endregion

        #region Property MapCounter
        /// <summary>
        /// The <see cref="MapCounter" /> property's name.
        /// </summary>
        public const string MapCounterPropertyName = "MapCounter";

        private int _MapCounter = 0;

        /// <summary>
        /// Sets and gets the MapCounter property.
        /// Changes to that property's value raise the PropertyChanged event. 
        /// </summary>
        public int MapCounter
        {
            get
            {
                return _MapCounter;
            }

            set
            {
                if (_MapCounter == value)
                {
                    return;
                }
                _MapCounter = value;
                RaisePropertyChanged(MapCounterPropertyName);
            }
        }
        #endregion

        #region Property NearBy_Restaurents_Headers
        /// <summary>
        /// The <see cref="NearByRestaurentHeader" /> property's name.
        /// </summary>
        public const string NearByRestaurentHeaderPropertyName = "NearByRestaurentHeader";

        private string _nearbyrestaurentsheader = "Restaurents";

        /// <summary>
        /// Sets and gets the NearByRestaurentHeader property.
        /// Changes to that property's value raise the PropertyChanged event. 
        /// </summary>
        public string NearByRestaurentHeader
        {
            get
            {
                return _nearbyrestaurentsheader;
            }

            set
            {
                if (_nearbyrestaurentsheader == value)
                {
                    return;
                }
                _nearbyrestaurentsheader = value;
                RaisePropertyChanged(NearByRestaurentHeaderPropertyName);
            }
        }
        #endregion

        #region Property MapCoOrdinateCollection
        /// <summary>
        /// The <see cref="MapCoOrdinateCollection" /> property's name.
        /// </summary>
        public const string MapCoOrdinateCollectionPropertyName = "MapCoOrdinateCollection";

        private ObservableCollection<MapCoOrdinate> _MapCoOrdinateCollection = new ObservableCollection<MapCoOrdinate>();

        /// <summary>
        /// Sets and gets the MapCoOrdinateCollection property.
        /// Changes to that property's value raise the PropertyChanged event. 
        /// </summary>
        public ObservableCollection<MapCoOrdinate> MapCoOrdinateCollection
        {
            get
            {
                return _MapCoOrdinateCollection;
            }

            set
            {
                if (_MapCoOrdinateCollection == value)
                {
                    return;
                }
                _MapCoOrdinateCollection = value;
                RaisePropertyChanged(MapCoOrdinateCollectionPropertyName);
            }
        }
        #endregion

        #region RelayCommand GotoDetailsPageCommand
        /// <summary>
        /// The <see cref="GotoDetailsPageCommand" /> property's name.
        /// </summary>
        public const string GotoDetailsPageCommandPropertyName = "GotoDetailsPageCommand";

        private RelayCommand<object> _gotoDetailsPageCommand = null;

        /// <summary>
        /// Sets and gets the GotoDetailsPageCommand property.
        /// Changes to that property's value raise the PropertyChanged event. 
        /// </summary>
        public RelayCommand<object> GotoDetailsPageCommand
        {
            get
            {
                return _gotoDetailsPageCommand;
            }

            set
            {
                if (_gotoDetailsPageCommand == value)
                {
                    return;
                }                
                _gotoDetailsPageCommand = value;
                RaisePropertyChanged(GotoDetailsPageCommandPropertyName);
            }
        }
        #endregion

        #region RelayCommand GotoMapPageCommand
        /// <summary>
        /// The <see cref="GotoMapPageCommand" /> property's name.
        /// </summary>
        public const string GotoMapPageCommandPropertyName = "GotoMapPageCommand";

        private RelayCommand _GotoMapPageCommand;

        /// <summary>
        /// Sets and gets the GotoMapPageCommand property.
        /// Changes to that property's value raise the PropertyChanged event. 
        /// </summary>
        public RelayCommand GotoMapPageCommand
        {
            get
            {
                return _GotoMapPageCommand;
            }

            set
            {
                if (_GotoMapPageCommand == value)
                {
                    return;
                }                
                _GotoMapPageCommand = value;
                RaisePropertyChanged(GotoMapPageCommandPropertyName);
            }
        }
        #endregion

        #region RelayCommand SearchKeyChanged
        /// <summary>
        /// The <see cref="SearchKeyChangedCommand" /> property's name.
        /// </summary>
        public const string SearchKeyChangedCommandPropertyName = "SearchKeyChangedCommand";

        private RelayCommand<object> _searchkeychanged = null;

        /// <summary>
        /// Sets and gets the SearchKeyChangedCommand property.
        /// Changes to that property's value raise the PropertyChanged event. 
        /// </summary>
        public RelayCommand<object> SearchKeyChangedCommand
        {
            get
            {
                return _searchkeychanged;
            }

            set
            {
                if (_searchkeychanged == value)
                {
                    return;
                }                
                _searchkeychanged = value;
                RaisePropertyChanged(SearchKeyChangedCommandPropertyName);
            }
        }
        #endregion

        #region RelayCommand LoadMapCoOrdinateNextCommand
        /// <summary>
        /// The <see cref="LoadMapCoOrdinateNextCommand" /> property's name.
        /// </summary>
        public const string LoadMapCoOrdinateNextCommandPropertyName = "LoadMapCoOrdinateNextCommand";

        private RelayCommand _LoadMapCoOrdinateNextCommand;

        /// <summary>
        /// Sets and gets the LoadMapCoOrdinateNextCommand property.
        /// Changes to that property's value raise the PropertyChanged event. 
        /// </summary>
        public RelayCommand LoadMapCoOrdinateNextCommand
        {
            get
            {
                return _LoadMapCoOrdinateNextCommand;
            }

            set
            {
                if (_LoadMapCoOrdinateNextCommand == value)
                {
                    return;
                }
                _LoadMapCoOrdinateNextCommand = value;
                RaisePropertyChanged(LoadMapCoOrdinateNextCommandPropertyName);
            }
        }
        #endregion
        #region RelayCommand LoadMapCoOrdinatePreviousCommand
        /// <summary>
        /// The <see cref="LoadMapCoOrdinatePreviousCommand" /> property's name.
        /// </summary>
        public const string LoadMapCoOrdinatePreviousCommandPropertyName = "LoadMapCoOrdinatePreviousCommand";

        private RelayCommand _LoadMapCoOrdinatePreviousCommand;

        /// <summary>
        /// Sets and gets the LoadMapCoOrdinatePreviousCommand property.
        /// Changes to that property's value raise the PropertyChanged event. 
        /// </summary>
        public RelayCommand LoadMapCoOrdinatePreviousCommand
        {
            get
            {
                return _LoadMapCoOrdinatePreviousCommand;
            }

            set
            {
                if (_LoadMapCoOrdinatePreviousCommand == value)
                {
                    return;
                }
                _LoadMapCoOrdinatePreviousCommand = value;
                RaisePropertyChanged(LoadMapCoOrdinatePreviousCommandPropertyName);
            }
        }
        #endregion
        public string HelloWorld { get; set; }
        public MainViewModel()
        {
            if (IsInDesignMode)
            {
                IsNotMainPivotVisible = true;
                IsDetectingLocation = true;
                HelloWorld = Token;
            }
            
            IsNotMainPivotVisible = IsInDesignMode ? false : true;
            GotoDetailsPageCommand = new RelayCommand<object>((id)=>GotoDetailsPage(id));
            SearchKeyChangedCommand = new RelayCommand<object>((id) => SearchKeyChangedMethod(id));
            GotoMapPageCommand = new RelayCommand(GotoMapPageMethod);
            LoadMapCoOrdinateNextCommand = new RelayCommand(LoadMapCoOrdinateNext);
            LoadMapCoOrdinatePreviousCommand = new RelayCommand(LoadMapCoOrdinatePrevious);
            
        }
        bool flag = false;
        public void LoadMapCoOrdinateNext()
        {
            if (MapCounter < RootObject.data.Count)
            {
                MapCoOrdinateCollection.Clear();
            }
            for (; MapCounter < RootObject.data.Count; MapCounter++)
            {
                if (MapCounter % 10 == 0 && MapCounter != 0 && flag)
                {
                    flag = false;
                    break;
                }    
                else
                {
                    MapCoOrdinate temp = new MapCoOrdinate();
                    temp.geopoint = RootObject.data[MapCounter].location.MapCoOrdinate;
                    temp.Name = RootObject.data[MapCounter].name;
                    temp.Id = RootObject.data[MapCounter].id;
                    MapCoOrdinateCollection.Add(temp);
                    if (MapCounter %10 == 0)
                    {
                        flag = true;
                    }
                }                    
            }
        }
        public void LoadMapCoOrdinatePrevious()
        {
            if (MapCounter == RootObject.data.Count)
            {
                MapCounter--;
            }
            MapCoOrdinateCollection.Clear();
            for (; MapCounter < RootObject.data.Count; MapCounter--)
            {
                if (MapCounter % 10 == 0 && MapCounter != 0 && flag)
                {
                    flag = false;
                    break;
                }
                else
                {
                    if (MapCounter == -1)
                    {
                        MapCounter++;
                        break;
                    }
                    MapCoOrdinate temp = new MapCoOrdinate();
                    temp.geopoint = RootObject.data[MapCounter].location.MapCoOrdinate;
                    temp.Name = RootObject.data[MapCounter].name;
                    temp.Id = RootObject.data[MapCounter].id;
                    MapCoOrdinateCollection.Add(temp);
                    if (MapCounter % 10 == 0)
                    {
                        flag = true;
                    }
                }
            }
        }

        public void SearchKeyChangedMethod(object id)
        {
            SelectedIndex = 0;
            MapCounter = 0;
            string key = (string)id;                        
            SearchKey = key;
            NearByRestaurentHeader = SearchKey;
            
        }
       
        public async void LoadData()
        {
            //DetectLocation();
            IsNotMainPivotVisible = true;
            IsDetectingLocation = true;
            HttpClient client = new HttpClient();
            string Url = @"https://graph.facebook.com/oauth/access_token?client_id=216542808535174&client_secret=040b79c3af2b7370bb9f17ffa46028bf&grant_type=client_credentials";
            try
            {
                Token = await client.GetStringAsync(Url);
                int tokenIndex = Token.IndexOf('=');
                Token = Token.Substring(tokenIndex + 1);


                //SearchKey = "american restaurant";
                Type = "Place";
                Distance = "50000";


                SearchUrl = @"https://graph.facebook.com/search?q=" + SearchKey
                                                                        + @"&type=" + Type
                                                                        + @"&center=" + MyCenter
                                                                        + @"&distance=" + Distance
                                                                        + @"&access_token=" + Token;

                try
                {
                    string rootobject = await client.GetStringAsync(SearchUrl);
                    RootObject = JsonConvert.DeserializeObject<RootObject>(rootobject);
                    RootObject.data = RootObject.data.OrderBy(p=>p.name).ToList();

                    foreach (var item in RootObject.data)
                    {
                        item.location.city = item.location.street + " " + item.location.city + " " + item.location.zip;
                    }
                    foreach (var item in RootObject.data)
                    {
                        BasicGeoposition basic = new BasicGeoposition() { Latitude = item.location.latitude, Longitude = item.location.longitude };
                        item.location.MapCoOrdinate = new Geopoint(basic);                        
                    }
                    LoadMapCoOrdinateNext();


                    BasicGeoposition position = new BasicGeoposition();
                    position.Latitude = MyBasicGeoPosition.Latitude;
                    position.Longitude = MyBasicGeoPosition.Longitude;

                    MyGeoPoint = new Geopoint(position);

                    MapLocationFinderResult result = await MapLocationFinder.FindLocationsAtAsync(MyGeoPoint);

                    // here also it should be checked if there result isn't null and what to do in such a case                    
                    CivicAddress = result.Locations[0].Address.StreetNumber + ", " + result.Locations[0].Address.Street + ", " + result.Locations[0].Address.Town + ", " + result.Locations[0].Address.Country;
                
                    IsShowErrorMessage = false;
                }
                catch (Exception e)
                {
                    ErroMeassage = e.Message;
                    IsShowErrorMessage = true;
                    SearchKey = "restaurants";
                    LoadData();
                }

                IsNotMainPivotVisible = false;
                IsDetectingLocation = false;
            }
            catch (Exception e)
            {
                if (e.Message == "net_http_message_not_success_statuscode")
                {
                    ErroMeassage = "There is something wrong with your data connection :( \nKindly check your wifi or cellular data";    
                }
                IsShowErrorMessage = true;
                
                LoadData();
            }
        }
        async void GotoMapPageMethod()
        {            
            Messenger.Default.Send<string>("","navigateToMap");
        }
        async void GotoDetailsPage(object id)
        {
            string ID = id.ToString();
            (App.Current.Resources["Locator"] as ViewModelLocator).PageDetails.PageID = ID;
            Messenger.Default.Send<string>(ID, "navigate");

        }
       
        public async void DetectLocation()
        {
            IsDetectingLocation = true;
            NearByRestaurentHeader = "Your Location";
            Geolocator geolocator = new Geolocator();
                geolocator.DesiredAccuracyInMeters = 50;

                try
                {
                    Geoposition geoposition;                        
                    geoposition = await geolocator.GetGeopositionAsync(maximumAge: TimeSpan.FromMinutes(5),timeout: TimeSpan.FromSeconds(10));
                    
                    MyCenter = geoposition.Coordinate.Latitude + "," + geoposition.Coordinate.Longitude;
                    
                    LoadData();
                    // reverse geocoding
                    MyBasicGeoPosition = new BasicGeoposition()
                    {
                        Longitude = geoposition.Coordinate.Longitude,
                        Latitude = geoposition.Coordinate.Latitude
                    };


                    //MyGeoPoint = new Geopoint(MyBasicGeoPosition); 

                    //MapLocationFinderResult result = await MapLocationFinder.FindLocationsAtAsync(MyGeoPoint);

                    //// here also it should be checked if there result isn't null and what to do in such a case                    
                    //CivicAddress = result.Locations[0].Address.StreetNumber + ", " + result.Locations[0].Address.Street + ", " + result.Locations[0].Address.Town + ", " + result.Locations[0].Address.Country;
                }
                catch (Exception ex)
                {
                    if ((uint)ex.HResult == 0x80004004)
                    {
                        // the application does not have the right capability or the location master switch is off
                        // "location  is disabled in phone settings.";
                    }                
                }
                
                NearByRestaurentHeader = "Nearby Restaurents";
        }
 
    }
}
