﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Nearest_Restaurent.Helpers
{
    public class BooleanCheck
    {

        public bool IsOpeningHourAvaiable { get; set; }

        public bool IsSpecialtiesAvailable { get; set; }

        public bool IsParkingAvailable { get; set; }

        public bool IsPaymentAvailable { get; set; }

        public bool IsServicesAvailable { get; set; }

        public bool IsAboutAvailable { get; set; }

        public bool IsPhoneNumberAvailable { get; set; }

        public bool IsWebsiteAvailable { get; set; }
    }
}
