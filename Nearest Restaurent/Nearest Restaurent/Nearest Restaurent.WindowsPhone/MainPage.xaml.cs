﻿using GalaSoft.MvvmLight.Messaging;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Phone.UI.Input;
using Windows.System;
using Windows.UI.Popups;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;


// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

namespace Nearest_Restaurent
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        public MainPage()
        {
            this.InitializeComponent();           
            LocationLoadingPermission();    
            this.NavigationCacheMode = NavigationCacheMode.Required;
            Messenger.Default.Register<string>(this, "navigate", (id) => navigate(id));
            Messenger.Default.Register<string>(this, "navigateToMap", (id) => navigateToMap(id));
            LoadingAnimation.Begin();
            HardwareButtons.BackPressed += HardwareButtons_BackPressed;
        }

        private void HardwareButtons_BackPressed(object sender, BackPressedEventArgs e)
        {
            e.Handled = true;
            Windows.Phone.UI.Input.HardwareButtons.BackPressed -= HardwareButtons_BackPressed;

            //App.Current.Exit();
            
        }


        public void navigateToMap(string kochu)
        {
            this.Frame.Navigate(typeof(Map));
        }
        
        private void navigate(string id)
        {
            this.Frame.Navigate(typeof(DetailsPage), id);
        }
         
        private async void LocationLoadingPermission()
        {
            var dialog = new MessageDialog("We need to detect your current location to find nearest restaurants");
            dialog.Title = "Really?";
            dialog.Commands.Clear();
            dialog.Commands.Add(new UICommand { Label = "Ok", Id = 0 });
            dialog.Commands.Add(new UICommand { Label = "Cancel", Id = 1 });
            var res = await dialog.ShowAsync();

            if ((int)res.Id == 0)
            {
                (App.Current.Resources["Locator"] as ViewModel.ViewModelLocator).Main.DetectLocation();  
            }
            else
            {
               
            }
        }

        /// <summary>
        /// Invoked when this page is about to be displayed in a Frame.
        /// </summary>
        /// <param name="e">Event data that describes how this page was reached.
        /// This parameter is typically used to configure the page.</param>
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            // TODO: Prepare page for display here.

            // TODO: If your application contains multiple pages, ensure that you are
            // handling the hardware Back button by registering for the
            // Windows.Phone.UI.Input.HardwareButtons.BackPressed event.
            // If you are using the NavigationHelper provided by some templates,
            // this event is handled for you.
        }

         
      

        private void StackPanel_Tapped(object sender, TappedRoutedEventArgs e)
        {
            LoadingAnimation.Begin();
        }

      

        private async void AppBarButton_Tapped(object sender, TappedRoutedEventArgs e)
        {
            string APP_ID = "77f63fd8-a200-47ab-b972-afe433d4cf86";
            await Launcher.LaunchUriAsync(new Uri("ms-windows-store:reviewapp?appid=" + APP_ID));
        }
    }
}
