﻿using Nearest_Restaurent.ViewModel;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Devices.Geolocation;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using Windows.ApplicationModel.Calls;
using Windows.Phone.UI.Input;
using System.ComponentModel;
using Windows.UI.Xaml.Controls.Maps;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkID=390556

namespace Nearest_Restaurent
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class DetailsPage : Page
    {
        public DetailsPage()
        {
            this.InitializeComponent();
            HardwareButtons.BackPressed += HardwareButtons_BackPressed;

            //MapIcon MapIcon1 = new MapIcon();
            //MapIcon1.Location = new Geopoint(new BasicGeoposition()
            //{
            //    Latitude = 47.620,
            //    Longitude = -122.349
            //});
            //MapIcon1.NormalizedAnchorPoint = new Point(0.5, 1.0);
            //MapIcon1.Title = "Space Needle";
            //Map.MapElements.Add(MapIcon1);

            //Map.Center = new Geopoint(new BasicGeoposition()
            //{
            //    Latitude = 47.620,
            //    Longitude = -122.349
            //});
        }
        void HardwareButtons_BackPressed(object sender, BackPressedEventArgs e)
        {
            e.Handled = true;
            Windows.Phone.UI.Input.HardwareButtons.BackPressed -= HardwareButtons_BackPressed;

            this.Frame.Navigate(typeof(MainPage));
        }
 
        /// <summary>
        /// Invoked when this page is about to be displayed in a Frame.
        /// </summary>
        /// <param name="e">Event data that describes how this page was reached.
        /// This parameter is typically used to configure the page.</param>
        
        //protected override void OnNavigatedTo(NavigationEventArgs e)
        //{
        //    string Id = (e.Parameter).ToString();
        //}
        //protected override void OnNavigatedFrom(NavigationEventArgs e)
        //{
        //    this.Frame.Navigate(typeof(MainPage));
        //}

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            this.Frame.Navigate(typeof(MainPage));
        }

        private void TextBlock_Tapped(object sender, TappedRoutedEventArgs e)
        {
            string number = (sender as TextBlock).Text;
            if (number.Length == 7)
            {
                number = "02" + number;
            }
            string Name = (App.Current.Resources["Locator"] as ViewModelLocator).PageDetails.PageDetailsProperty.name;
            PhoneCallManager.ShowPhoneCallUI(number, Name);
        }
    }
}
